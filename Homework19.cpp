﻿#include <iostream>
#include <string>

using namespace std;

class Animal
{
public:
    virtual ~Animal() {}
    virtual string Voice() = 0;
};

class Dog : public Animal
{
public:
    string Voice() override
    {
        return "Woof!";
    }
};

class Cow : public Animal
{
public:
    string Voice() override
    {
        return "Moo!";
    }
};

class Cat : public Animal
{
public:
    string Voice() override
    {
        return "Meow!";
    }
};

int main()
{
    Animal* animals[3] = {
        new Dog(),
        new Cow(),
        new Cat()
    };
    
    for (Animal* animal : animals) {
        cout << animal->Voice() << "\n";
        delete animal;
    }

    return 0;
}
